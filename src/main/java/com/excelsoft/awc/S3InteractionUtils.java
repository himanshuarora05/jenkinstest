package com.excelsoft.awc;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.transfer.Copy;
import com.amazonaws.services.s3.transfer.TransferManager;

public class S3InteractionUtils {

	public static String bucketName = "s3-ngl.excelindia.com";

	public static String accesskey = "AKIAJYU7B5MJGBDMYZEA";

	public static String secretKey = "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";

	public static String expirationTime = "60";

	public static String s3Protocol = "http";

	public static long defaultFilePartSize = 5242880;

	public static int defaultPoolSize = 30;

	public static int defaultThreadTimeout = 60;

	public static boolean upload(String srcFileAbsolutePath,
			String contentSystemDestinationPath) throws Exception {

		System.out.println("upload" + srcFileAbsolutePath + ">>>"
				+ contentSystemDestinationPath);
		if (StringUtils.isEmpty(srcFileAbsolutePath)) {
			throw new IllegalArgumentException("source cannot be null or empty");
		}
		ExecutorService pool = null;
		AmazonS3 s3Client = null;
		BasicAWSCredentials credentials = new BasicAWSCredentials(accesskey,
				secretKey);
		try {
			s3Client = new AmazonS3Client(credentials);

			// Create fixed Thread pool, here pool of 2 thread will created
			pool = Executors.newFixedThreadPool(defaultPoolSize);

			// Create objects of Runnable
			try {
				String keyName = contentSystemDestinationPath + "/"
						+ FilenameUtils.getName(srcFileAbsolutePath);
				UploadObjectMPULowLevelAPI uploadWorker = new UploadObjectMPULowLevelAPI(
						srcFileAbsolutePath, keyName, s3Client, bucketName,
						defaultFilePartSize);
				uploadWorker.run();
			} catch (RuntimeException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (pool != null) {
				System.out.println("multiThreadUpload End Executer");
				pool.shutdown();
				pool.awaitTermination(defaultThreadTimeout, TimeUnit.MINUTES);

			}
		}
		return true;
	}

	public static boolean copyFile(String source, String destination) {

		TransferManager transferManager = getTransferManager();
		try {
			Copy copy =  transferManager.copy(bucketName, source, bucketName,
					destination); 
			if(copy.waitForCopyResult()!=null){
				System.out.println("Copy Description " + copy.getDescription());
				return true;
			}
		} catch (AmazonServiceException exception) {
			System.out.println("Error Message: " + exception);
		} catch (AmazonClientException ace) {
			System.out
					.println("Internal error while trying to communicate with S3");
			System.out.println("Error Message: " + ace);
		} catch (Exception ex) {
			System.out.println("Error Message: " + ex);
		} finally {
			transferManager.shutdownNow();
		}
		return false;
	}

	public static TransferManager getTransferManager() {
		BasicAWSCredentials credentials = new BasicAWSCredentials(accesskey,
				secretKey);
		TransferManager tx = new TransferManager(credentials);
		return tx;
	}

}

package com.excelsoft.awc;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;

public class S3Upload {

	private static final String ANY_FILE = "anyFile.mp3";

	private static String FILE_NAME_1 = "D:" + File.separator + ANY_FILE;
	
	private static String FILE_NAME_2 = "D:" + File.separator + ANY_FILE;

	public static String s3Folder = "OPS_CONTENT1";

	private static String UPLOAD_PATH_1 = s3Folder + "/resources";
	
	private static String UPLOAD_PATH_2 = s3Folder + "/book_data";
	
	public static void main(String[] args) throws Exception {
		
		File file = new File(FILE_NAME_1);
		long startTime = System.currentTimeMillis();
		String md5_1 = getMD5(file);
		long endTime  = System.currentTimeMillis();
		
		System.out.println("===========MD5_1===========" + md5_1);
		System.out.println("Time Taken to calculate MD5_1 of size "
				+ (file.length()/1000)/1000 + " MB is " + (endTime - startTime)/1000);

		startTime = System.currentTimeMillis();
		S3InteractionUtils.upload(FILE_NAME_1, UPLOAD_PATH_1);
		endTime  = System.currentTimeMillis();
		
		System.out.println("Time Take by uploading file on Amazon S3 of size = "
				+ (file.length()/1000)/1000 + " MB is " + ((endTime - startTime)/1000));
		
		file = new File(FILE_NAME_2);
		startTime = System.currentTimeMillis();
		String md5_2 = getMD5(file);
		endTime  = System.currentTimeMillis();
		
		System.out.println("===========MD5_2===========" + md5_2);
		System.out.println("Time Taken to calculate MD5_2 of size "
				+ (file.length()/1000)/1000 + " MB is " + (endTime - startTime)/1000);
		
		if(md5_1.equals(md5_2)){
			startTime = System.currentTimeMillis();
			S3InteractionUtils.copyFile(UPLOAD_PATH_1 + "/" + ANY_FILE, UPLOAD_PATH_2 + "/" + ANY_FILE);
			endTime  = System.currentTimeMillis();
			System.out.println("Time Take by copying file on Amazon S3 of size = "
					+ (file.length()/1000)/1000 + " MB is " + (endTime - startTime)/1000);
		}else{
			System.out.println("==NOT POSSIBLE==MD5 must be same");
		}
	}
	
	private static String getMD5(File file) throws Exception{
        MessageDigest md = MessageDigest.getInstance("MD5");
        FileInputStream fis = new FileInputStream(file);
        byte[] dataBytes = new byte[1024];

        int nread = 0;
        while ((nread = fis.read(dataBytes)) != -1) {
          md.update(dataBytes, 0, nread);
        };
        byte[] mdbytes = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        System.out.println("Digest(in hex format):: " + sb.toString());

        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<mdbytes.length;i++) {
    		String hex=Integer.toHexString(0xff & mdbytes[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	return hexString.toString();
	}
}

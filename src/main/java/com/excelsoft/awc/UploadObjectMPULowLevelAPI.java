package com.excelsoft.awc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.tika.Tika;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AbortMultipartUploadRequest;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.UploadPartRequest;

/**
 * @author IMFCORP\vivek.bharti
 *
 */
public class UploadObjectMPULowLevelAPI implements Runnable{

	//File folder to be uploaded
	private String filePath;
	//Key of the object on S3
	private String keyName;
	//S3 Bucket Name
	private String bucketName;
	//File part size in bytes to be uploaded in every request
	private long filePartSize;
	//S3Client to be used
    private AmazonS3 s3Client;
    
	/**
	 * @param filePath
	 * @param keyName
	 * @param s3Client
	 * @param bucketName
	 * @param filePartSize
	 * Constructor to initialize the thread values
	 */
	UploadObjectMPULowLevelAPI(String filePath, String keyName, AmazonS3 s3Client, String bucketName, long filePartSize) {
		this.filePath = filePath;
		this.keyName = keyName;
		this.s3Client = s3Client;
		this.bucketName = bucketName;
		this.filePartSize = filePartSize;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
        // Create a list of UploadPartResponse objects. You get one of these
        // for each part upload.
        List<PartETag> partETags = new ArrayList<PartETag>();
        
        File file = new File(filePath);
        long contentLength = file.length();
        
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(contentLength);
        meta.setContentType(getContentType(file));

        // Step 1: Initialize.
        InitiateMultipartUploadRequest initRequest = new 
             InitiateMultipartUploadRequest(bucketName, keyName, meta);
        InitiateMultipartUploadResult initResponse = 
        	                   s3Client.initiateMultipartUpload(initRequest);

        long partSize = filePartSize;

        try {
            // Step 2: Upload parts.
            long filePosition = 0;
            for (int i = 1; filePosition < contentLength; i++) {
                // Last part can be less than 5 MB. Adjust part size.
            	partSize = Math.min(partSize, (contentLength - filePosition));
            	
                // Create request to upload a part.
                UploadPartRequest uploadRequest = new UploadPartRequest()
                    .withBucketName(bucketName).withKey(keyName)
                    .withUploadId(initResponse.getUploadId()).withPartNumber(i)
                    .withFileOffset(filePosition)
                    .withFile(file)
                    .withPartSize(partSize);
                
                // Upload part and add response to our list.
                partETags.add(
                		s3Client.uploadPart(uploadRequest).getPartETag());
                System.out.println("Uploaded Part " + partSize);
                filePosition += partSize;
            }

            // Step 3: Complete.
            CompleteMultipartUploadRequest compRequest = new 
                         CompleteMultipartUploadRequest(
                        		 bucketName, 
                                    keyName, 
                                    initResponse.getUploadId(), 
                                    partETags);
            //Complete a multipart upload by assembling previously uploaded parts.
            s3Client.completeMultipartUpload(compRequest);
        } catch (Exception e) {
            s3Client.abortMultipartUpload(new AbortMultipartUploadRequest(
            		bucketName, keyName, initResponse.getUploadId()));
            throw new RuntimeException("UploadObjectMPULowLevelAPI: Error occured uploading : " + keyName);
        }
    }

	private String getContentType(File file) {
		String contentType = "";
        Tika tika = new Tika();
        try {
			contentType = tika.detect(file);
			if(file.getAbsolutePath().endsWith("html") || file.getAbsolutePath().endsWith("htm")){
				contentType = "text/html";
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return contentType;
	}
}
